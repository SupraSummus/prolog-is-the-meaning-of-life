quickSort([], []).
quickSort([Pivot | Before], After) :-
	quickSortPartition(Before, Pivot, PartA, PartB),
	quickSort(PartA, SortedPartA),
	quickSort(PartB, SortedPartB),
	append(SortedPartA, [Pivot | SortedPartB], After).

quickSortPartition([], _, [], []).
quickSortPartition([A | Before], Pivot, [A | PartA], PartB) :-
	A =< Pivot,
	quickSortPartition(Before, Pivot, PartA, PartB).
quickSortPartition([A | Before], Pivot, PartA, [A | PartB]) :-
	A > Pivot,
	quickSortPartition(Before, Pivot, PartA, PartB).
