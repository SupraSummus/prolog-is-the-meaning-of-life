% Dany jest graf skierowany, reprezentowany klauzulowo za pomocą list sąsiedztwa,
% czyli dany jest predykat:
%    neighbours(Wierzchołek, ListaSąsiadów).
%
% Zdefiniować predykat:  cykle(V, W), który dla podanego zbioru V, będącego podzbiorem zbioru wierzchołków grafu, znajdzie największy jego podzbiór W, złożony z takich wierzchołków, z których istnieje ścieżka (niepusty ciąg krawędzi) do nich samych.
%
% Na każde zapytanie, w którym pierwszy argument jest ustaloną listą, a drugi jest zmienną powinna zostać udzielona dokładnie jedna odpowiedź.
%
% Przykładowy graf:

neighbours(a, [b,c,d]).
neighbours(b, [e]).
neighbours(c, [d]).
neighbours(d, [a]).
neighbours(e, [e]).

% Przykładowe zapytania i (przykładowe) odpowiedzi:
%
% | ?- cykle([a,b,c], W).
%      W = [a,c];
%      no
%
% | ?- cykle([a,b,c,d,e], W).
%      W = [a,c,d,e]
%
% | ?- cykle([a,b], W).
%      W = [a]
%
% | ?- cykle([a,b,e], W).
%      W = [a,e]

cykle(V, W) :- cykle(V, [], W).

% cykle(Z, )
cykle([], W, W).
cykle([V | VV], W, WW) :- osiagalny(V, V), !, cykle(VV, [V | W], WW).
cykle([V | VV], W, WW) :- \+ osiagalny(V, V), cykle(VV, W, WW).

% osiagalny(A, B) == z A da sie dojsc do B niezerowa liczba krawedzi
% konstrukcja eliminujaca wiele sukcesów
osiagalny(A, B) :- (osiagalny(A, B, [A]) -> true; false).

% osiagalny(A, B, Odwiedzone) == z A da sie dojsc do B niezerowa liczba krawedzi nie wchodzac w wierzcholki z listy Odwiedzone.
osiagalny(A, B, _) :- neighbour(A, B).
osiagalny(A, C, Odwiedzone) :- neighbour(A, B), \+ member(B, Odwiedzone), osiagalny(B, C, [B | Odwiedzone]).

% neighbour(A, B) == jest krawedz A->B
neighbour(A, B) :- neighbours(A, AA), member(B, AA).

