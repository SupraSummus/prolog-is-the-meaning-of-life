quickSort(Before, After) :- quickSortAndPrepend(Before, [], After).

quickSortAndPrepend([], Tail, Tail).
quickSortAndPrepend([Pivot | Before], Tail, SortedAndTail) :-
	quickSortPartition(Before, Pivot, PartA, PartB),
	quickSortAndPrepend(PartB, Tail, PartBAndTail),
	quickSortAndPrepend(PartA, [Pivot | PartBAndTail], SortedAndTail).

quickSortPartition([], _, [], []).
quickSortPartition([A | Before], Pivot, [A | PartA], PartB) :-
	A =< Pivot,
	quickSortPartition(Before, Pivot, PartA, PartB).
quickSortPartition([A | Before], Pivot, PartA, [A | PartB]) :-
	A > Pivot,
	quickSortPartition(Before, Pivot, PartA, PartB).
