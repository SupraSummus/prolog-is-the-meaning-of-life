% 1. Reprezentacja liczb naturalnych: 0, s/1.
%    Zdefiniuj predykaty odd/1, even/1 zachodzące wtw, gdy argument jest
%    liczbą (odpowiednio) (nie)parzystą:
%       a) jako program bez negacji
%       b) jako różne programy z negacją (warstwowe, niewarstwowe).
odd(s(0)).
odd(s(s(X))) :- odd(X).

even(0).
even(s(s(X))) :- even(X).

even1(X) :- \+ odd(X).

even2(0).
even2(s(X)) :- \+ even2(X).

odd2(s(0)).
odd2(s(X)) :- \+ odd2(X).

% 2. Informacje o pewnej funkcji, o skończonej dziedzinie i
%    przeciwdziedzinie D, są dane w postaci definicji predykatu:
%         wf(x,y) <==> f(x)=y.
%    Zdefiniuj predykat fna/? zachodzący wtw, gdy funkcja zdefiniowana
%    predykatem wf/2 jest "na".
%    Dziedzina funkcji może być reprezentowana jako lista elementów.
%    Napisz program bez negacji oraz program z negacją.

wf(a, b).
wf(b, b).
wf(c, a).

% fna(D) == f jest na D
fna([]).
fna([X | D]) :- wf(_, X), fna(D).

% fna == f jest na
fna :- \+ niefna.
niefna :- wf(X, _), \+ osiagalny(X).
osiagalny(X) :- wf(_, X).
