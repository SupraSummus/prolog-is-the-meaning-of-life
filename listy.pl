% lista(L): L jest listą
lista([]).
lista([ _ | Ogon ]) :- lista(Ogon).

% pierwszy(E, L): E jest pierwszym elementem L
pierwszy(Element, [ Element | Ogon ]) :- lista(Ogon).

% ostatni(E, L): E jest ostatnim elementem L
ostatni(Element, [Element]).
ostatni(Element, [ _ | Ogon ]) :- ostatni(Element, Ogon).

% element(E, L): E jest elementem L
element(Element, [Element | Ogon]) :- lista(Ogon).
element(Element, [_ | Ogon]) :- element(Element, Ogon).

% intersection(A, B): A ma czesc wspolna z B
intersection(A, B) :- element(E, A), element(E, B).

% scal(A, B, C): C jest skonkatenowanymi A i B
scal([], X, X).
scal([E | Ab], B, [E | Cb]) :- scal(Ab, B, Cb).

% odwroc(L, RL): L jest odwróconą RL
odwroc(A, B) :- odwroc(A, [], B).

odwroc([], A, A).
odwroc([E | A], B, C) :- odwroc(A, [E | B], C).

%  a) srodek(E, L) wtw, gdy E jest środkowym elementem listy L
%       (L jest nieparzystej długości)
%       czyli (k+1)-ym elementem, gdzie 2k+1 = długość listy L
%  b) wypisz(L) == czytelne wypisanie wszystkich elementów listy L
%       (elementy oddzielone przecinkami, na końcu kropka, lub CR,
%        info gdy lista pusta)
%  c) podziel(Lista, NieParz, Parz) wtw, gdy NieParz (odp. Parz) jest
%     listą zawierającą wszystkie elementy listy Lista znajdujące się na
%     miejscach o nieparzystych (odp. parzystych) indeksach

% srodek(E, L): E jest srodkowym elementem B
srodek(X, [X]).
srodek(E, [_ | L]) :- append(LL, [_], L), srodek(E, LL).

% wypisz
wypisz([E]) :- write(E), write('\n').
wypisz([E | L]) :- write(E), write(', '), wypisz(L).
