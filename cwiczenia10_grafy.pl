% Reprezentacja grafów (skierowanych, nieskierowanych, etykietowanych itp.)
%   a) termowa: listy krawędzi, listy/macierze sąsiedztwa
%   b) klauzulowa
%       zbiór klauzul unarnych = zbiór krawędzi, zbiór list sąsiedztwa itd.

% 1. Rozważmy grafy typu DAG (skierowane acykliczne).
% ===================================================
%    Zdefiniuj predykaty:
%     a) connect(A, B), connect(Graf, A, B) <==> istnieje ścieżka z A do B,
%        tzn. istnieje niepusty ciąg krawędzi prowadzących z A do B
%        (dwie wersje: dwie reprezentacje grafu - termowa i klauzulowa, zbiory krawędzi)
connect([kr(A, B) | _], A, B).
connect([_ | Graf], A, B) :- connect(Graf, A, B).
connect([kr(A, B) | Graf], A, C) :- connect(Graf, B, C).
connect([kr(B, C) | Graf], A, C) :- connect(Graf, A, B).
connect([kr(B, C) | Graf], A, D) :- connect(Graf, A, B), connect(Graf, C, D).

%     b) path(A,B,P) <==> P = ścieżka z A do B (jw.),
%        tzn. P = [A, ..., B], czyli lista wierzchołków kolejnych krawędzi.
path(Graf, A, B, [A | Path]) :- path(Graf, A, B, [], Path).

% path(Graf, A, B, Path, AB_Path) == AB_Path jest konkatencją sciezki z A do B (bez wierzcholka A) w grafie Graf i listy Path
path([kr(A, B) | _], A, B, Path, [B | Path]).
path([kr(A, B) | Graf], A, C, Path, [B | BC_Path]) :- path(Graf, B, C, Path, BC_Path).
path([kr(B, C) | Graf], A, C, Path, AB_BC_Path) :- path(Graf, A, B, [C | Path], AB_BC_Path).
path([kr(B, C) | Graf], A, D, Path, AB_BC_CD_Path) :-
	path(Graf, C, D, Path, CD_Path),
	path(Graf, A, B, [C | CD_Path], AB_BC_CD_Path).
path([_ | Graf], A, B, Path, AB_Path) :- path(Graf, A, B, Path, AB_Path).

% 2. Grafy skierowane (cykliczne).
% ================================
%    Zdefiniuj predykat:   pathC(A,B,P) <==> P ścieżka z A do B (jw.),

% TODO?

% 3. Grafy nieskierowane. (implementacja dla grafów skierowanych bo są ogólniejsze)
% =================================================================================
%    Zdefiniuj predykat euler/k (k >= 1) odnoszący sukces wtw, gdy podany
%    graf jest grafem Eulera i parametr P jest ścieżką Eulera w tym grafie
%    (tj. ścieżką przechodzącą przez każdą krawędź tego drzewa dokładnie raz).
eulerCykl(Graf, Path) :- euler(Graf, A, A, Path).
euler(Graf, A, B, [A | Path]) :- sciezka(Graf, [], A, B, [], Path).

% sciezka(Graf, Pozostale, A, B, Path, AB_Path) ==
%   AB_Path jest konkatenacja (sciezki [taka, ktora uzywa krawedzi co najwyzej jednokrotnie] z wierzcholka A do B w grafie Graf) i (listy Path)
%   Pozostale to krawędzie grafu Graf nieużyte w scieżce
sciezka(Graf, Graf, A, A, Path, Path).
sciezka(Graf, GrafBez_AB_BC, A, C, Path, [B | BC_Path]) :-
	sasiad(Graf, GrafBez_AB, A, B),
	sciezka(GrafBez_AB, GrafBez_AB_BC, B, C, Path, BC_Path).

% sasiad(Graf, Pozostale, A, B) ==
%   w grafie Graf istnieje krawedz A->B
%   Graf = Pozostale + ta krawedz
sasiad([kr(A, B) | Graf], Graf, A, B).
sasiad([E | AB_Graf], [E | Graf], A, B) :-
	sasiad(AB_Graf, Graf, A, B).

% 4. Grafy skierowane reprezentujemy jako listy krawędzi, czyli listy termów
% ==========================================================================
%    postaci kr(A,B) oznaczających krawedź z A do B.
%    Reprezentacja drzew binarnych: nil - drzewo puste,
%      tree(Lewe,Wierzch,Prawe) - drzewo niepuste.
% 
%    Napisać predykat drzewo(Graf, Drzewo), który dla w pełni ustalonego
%    termu Graf, reprezentującego graf skierowany, odniesie sukces wtw, gdy
%    graf ten jest drzewem binarnym i uzgodni zmienną Drzewo z reprezentacją
%    drzewa binarnego odpowiadającego grafowi Graf.

drzewo(Graf, Drzewo) :-
	korzen(Graf, Korzen),
	drzewo(Graf, Korzen, Drzewo, []).

korzen(Graf, K) :- korzen(Graf, Graf, K).
korzen([kr(A, _) | _], Graf, A) :- \+ cel(Graf, A).
korzen([kr(A, _) | Graf], GrafCaly, Korzen) :- 
	A \= Korzen,
	korzen(Graf, GrafCaly, Korzen).

cel([kr(_, W) | _], W).
cel([_ | Graf], W) :- cel(Graf, W). 

% drzewo(Graf, Korzen, Drzewo, Reszta) ==
%   Drzewo ma korzen Korzen i
%   Drzewo jest zbudowane z grfau Graf (lista termów postaci kr(A, B)) i
%   Reszta to lista niewykorzystanych termów kr(A, B)
drzewo(Graf, Korzen, tree(nil, Korzen, nil), Graf).
drzewo(Graf, K, tree(Lewe, K, Prawe), GrafBez_KL_KP_L_P) :-
	sasiad(Graf, GrafBez_KL, K, KL),
	sasiad(GrafBez_KL, GrafBez_KL_KP, K, KP),
	drzewo(GrafBez_KL_KP, KL, Lewe, GrafBez_KL_KP_L),
	drzewo(GrafBez_KL_KP_L, KP, Prawe, GrafBez_KL_KP_L_P).
drzewo(Graf, K, tree(Lewe, K, nil), GrafBez_KL_L) :-
	sasiad(Graf, GrafBez_KL, K, KL),
	drzewo(GrafBez_KL, KL, Lewe, GrafBez_KL_L).
drzewo(Graf, K, tree(nil, K, Lewe), GrafBez_KL_L) :-
	sasiad(Graf, GrafBez_KL, K, KL),
	drzewo(GrafBez_KL, KL, Lewe, GrafBez_KL_L).
