% sposób reprezentacji:
%   stała zero
%   funkcja następnika s/1

% nat(x) == x jest liczba naturalna
nat(zero).
nat(s(X)) :- nat(X).

% plus(x,y,z) == x + y = z (w dziedzinie N)
plus(A, zero, A).
plus(A, s(B), s(C)) :- plus(A, B, C).

% minus(x, y, z) == x - y = z
minus(A, B, C) :- plus(B, C, A).

% fib(k, x) == x jest k-ta liczba Fibonacciego
fib(zero, zero). 
fib(s(zero), s(zero)).
fib(s(s(K)), X) :-
	fib(K, A),
	fib(s(K), B),
	plus(A, B, X).

% le(x, y) ==  x <= y  (w dziedzinie liczb naturalnych)
le(zero, _).
le(s(X), s(Y)) :- le(X, Y).

% lt(x, y) ==  x < y  (w dziedzinie liczb naturalnych)
lt(X, Y) :- le(s(X), Y).

% razy(x, y, z) == x * y = z
razy(_, zero, zero).
razy(A, s(B), C) :- razy(A, B, X), plus(A, X, C).

% exp(n, x, z) == x^n = z
% silnia(n, s) == s = n!
% mod(x, y, z) == x modulo y = z
% nwd(x, y, z) == z = największy wspólny dzielnik x i y
