% mergeSort(list, sortedList) 
mergeSort([], []).
mergeSort([A], [A]).
mergeSort([A, B | Before], After) :-
	mergeSortPartition([A, B | Before], FirstHalf, SecondHalf),
	mergeSort(FirstHalf, FirstHalfSorted),
	mergeSort(SecondHalf, SecondHalfSorted),
	mergeSortMerge(FirstHalfSorted, SecondHalfSorted, After).

% mergeSortPartition(wholeList, partA, partB)
mergeSortPartition([], [], []).
mergeSortPartition([A], [A], []).
mergeSortPartition([A, B | ToBeDivided], [A | PartA], [B | PartB]) :-
	mergeSortPartition(ToBeDivided, PartA, PartB).

% mergeSortPartition(partB, partA, merged)
mergeSortMerge([], [], []).
mergeSortMerge([A | Part], [], [A | Part]).
mergeSortMerge([], [A | Part], [A | Part]).
mergeSortMerge([A | PartA], [B | PartB], [A | Whole]) :-
	A =< B,
	mergeSortMerge(PartA, [B | PartB], Whole).
mergeSortMerge([A | PartA], [B | PartB], [B | Whole]) :-
	B < A,
	mergeSortMerge([A | PartA], PartB, Whole).
