% Podstawowe operacje na drzewach binarnych
% -----------------------------------------

% Reprezentacja (termowa) drzew binarnych:
%   nil                  - drzewo puste
%   tree(Lewe, W, Prawe) - drzewo niepuste

% drzewo(D) == D jest drzewem binarnym
drzewo(nil).
drzewo(tree(Lewe, _, Prawe)) :- drzewo(Lewe), drzewo(Prawe).

% ileW(D, K) == drzewo binarne D składa się z K wierzchołków
ileW(D, K) :- ileW(D, 0, K).

ileW(nil, K, K).
ileW(tree(Lewe, _, Prawe), Przed, Po) :-
	PrzedIJeden is Przed + 1,
	ileW(Lewe, PrzedIJeden, PoLewym),
	ileW(Prawe, PoLewym, Po).

% insertBST(D, E, N) == N jest drzewem BST powstałym z (drzewa BST) D przez dodanie E
% procedura może dulikowac elementy
insertBST(nil, E, tree(nil, E, nil)).
insertBST(tree(Lewe, W, Prawe), E, tree(LeweE, W, Prawe)) :-
	E =< W, !,
	insertBST(Lewe, E, LeweE).
insertBST(tree(Lewe, W, Prawe), E, tree(Lewe, W, PraweE)) :-
	W < E,
	insertBST(Prawe, E, PraweE).

% createBST(L, D) == D jest drzewem BST zawierającym wszystkie wartości z listy L
createBST(L, D) :- createBST(L, nil, D).

createBST([], D, D).
createBST([E | Ogon], D, DPo) :-
	insertBST(D, E, DE),
	createBST(Ogon, DE, DPo).

% wypiszBST(D) == wypisanie wszystkich elementów drzewa BST (porządek infiksowy; prosto, czytelnie)

wypiszBST(nil).
wypiszBST(tree(L, W, P)) :- wypiszBST(L), print(W), print(' '), wypiszBST(P).

wypiszCoolBST(D) :- wypiszCoolBST(D, 0).
wypiszCoolBST(nil, _).
wypiszCoolBST(tree(L, W, P), K) :-
	K1 is K + 1,
	wypiszCoolBST(P, K1),
	wypiszWciecie(K), print(W), print('\n'),
	wypiszCoolBST(L, K1).

wypiszWciecie(0).
wypiszWciecie(K) :-
	K > 0,
	K1 is K - 1,
	print('    '),
	wypiszWciecie(K1).


% wypiszBST(D, L) == L = lista wszystkich elementów drzewa BST (porządek infiksowy)

wypiszBST(D, L) :- wypiszBST(D, [], L).

wypiszBST(nil, L, L).
wypiszBST(tree(Lewe, W, Prawe), Ogon, LeweWPraweOgon) :-
	wypiszBST(Prawe, Ogon, PraweOgon),
	wypiszBST(Lewe, [W | PraweOgon], LeweWPraweOgon).

% sortBST(L, S) == S = wynik sortowania L przy użyciu drzew BST
sortBST(L, S) :- createBST(L, D), wypiszBST(D, S).

% liscie(D, L) == L = lista wszystkich liści drzewa D (kolejność od lewej do prawej)
liscie(D, L) :- liscie(D, [], L).

liscie(nil, L, L).
liscie(tree(nil, W, nil), Ogon, [W | Ogon]).
liscie(tree(L, _, P), Ogon, LPOgon) :-
	(L, P) \= (nil, nil),
	liscie(P, Ogon, POgon),
	liscie(L, POgon, LPOgon).

% Zadanie domowe:  deleteBST(D, E, ND).
