% Jan Rydzewski

% podlisty(P, PP) == P jest skonkantenowanymi elementami listy list PP
%     P jest listą ustaloną
podlisty([], []).
podlisty([E | P], PP) :- podlisty(P, [E], E, PP).

% podlisty(P, Aktulana, Ostatni, PP) ==
%     lista Aktualna^(-1) + P jest skonkatenowaną listą list PP i
%     Aktualna ma conajmniej 1 element i jej ostatnim elementem jest Ostatni

%     przypadek gdy skończyło się wejście (lista P)
%     wrzucamy Aktualną do PP o ile ma odpowiednią postać
podlisty([], [E | Aktualna], E, [RAktualna]) :-
	odwroc([E | Aktualna], RAktualna).

%     zawsze możemy wziąć głowę P i wrzucić do Aktualnej
podlisty([E | P], Aktualna, Ostatni, PP) :-
	podlisty(P, [E | Aktualna], Ostatni, PP).

%     jeśli Aktualna ma odpowiednią postać to wrzucamy ją do PP
podlisty([EE | P], [E | Aktualna], E, [RAktualna | PP]) :-
	odwroc([E | Aktualna], RAktualna),
	podlisty(P, [EE], EE, PP).

% odwroc(L, RL) == L jest odwróconą RL
odwroc(A, B) :- odwroc(A, [], B).
odwroc([], A, A).
odwroc([E | A], B, C) :- odwroc(A, [E | B], C).
