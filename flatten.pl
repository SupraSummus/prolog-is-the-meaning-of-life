% flatten(LL, L) ==
% lista LL jest dowolnie zagnieżdżoną listą list,
% lista L jest płaską listą wszystkich elementów z listy LL (w tej samej kolejności).

flatten(LL, L) :- flatten(LL, [], L).

flatten([], Tail, Tail).
flatten([A | Rest], Tail, FlattenedA_FlattenedRest_Tail) :-
	is_list(A),
	flatten(Rest, Tail, FlattenedRest_Tail),
	flatten(A, FlattenedRest_Tail, FlattenedA_FlattenedRest_Tail).
flatten([A | Rest], Tail, [A | FlattenedRest_Tail]) :-
	not(is_list(A)),
	flatten(Rest, Tail, FlattenedRest_Tail).
